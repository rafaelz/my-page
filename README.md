# my-page
## simple project using HTML5 and CSS3.

# Sobre este projeto.

Este projeto cria de forma simples com HTML5 e CSS3,
uma simples pagina da web de apresentação pessoal.

## Erro x aprendizado

Como experiencia pessoal de aprendizado sobre este assunto,
tive uma situação curiosa desenvolvendo este projeto;
=============================================================

Este codigo aqui deveria abrir meu menu hamburger aplicando um efeito especifico;

#toggle:checked + .checkbox {
    background-color: white;   
}

#toggle:checked ~ .menu{
    box-shadow: 0px 0px 0px 100vmax white;
    z-index: 1;
}

#toggle:checked ~ .menu-items {
    visibility: visible;
    opacity: 1;
}

=========================================================================

Porem, o codigo abaixo era a maneira como codeie primeiramente e o acima,
a forma correta;


#toggle:checked + .checkbox {
    background-color: white;   
}

#toggle:checked + .menu{
    box-shadow: 0px 0px 0px 100vmax white;
    z-index: 1;
}

#toggle:checked + .menu-items {
    visibility: visible;
    opacity: 1;
}

A simples troca do sinal + por ~ resolveu o problema,
fica a dica pra quem esta tendo problemas aqui.

=========================================================================
